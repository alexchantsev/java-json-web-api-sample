package Domain;

import Domain.Models.Product;
import Helpers.HttpHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by aleksandr.chancev on 08.02.2016.
 */
public class ProductsRepository {
    public Collection<Product> getAll() throws IOException{
        String json = HttpHelper.getJson("http://esbsrv01/WebApiTest/api/Products/");
        Gson gson = new Gson();
        Type productCollectionType = new TypeToken<Collection<Product>>(){}.getType();
        Collection<Product> data = gson.fromJson(json, productCollectionType);
        return  data;
    }

    public Product get(int id) throws IOException {
        String json = HttpHelper.getJson("http://esbsrv01/WebApiTest/api/Products/"+ Integer.toString(id));
        Gson gson = new Gson();
        Product data = gson.fromJson(json, Product.class);
        return  data;
    }

    public void insert(String name) {
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Product product = new Product();
            product.Name = name;
            String json = gson.toJson(product);
            String response = HttpHelper.postJson("http://esbsrv01/WebApiTest/api/Products", json);
            System.out.printf(response);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void update(int id, String name) throws IOException{
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Product product = new Product();
        product.Id = id;
        product.Name = name;
        String json = gson.toJson(product);
        HttpHelper.uploadJson("http://esbsrv01/WebApiTest/api/Products/" + Integer.toString(id), json, "PUT");
    }
}
