package Helpers;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by aleksandr.chancev on 05.02.2016.
 */
public class HttpHelper {
    public static String getJson(String targetUrl) throws IOException {
        BufferedReader reader = null;
        try {
            URL url = new URL(targetUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("content-type", "application/json");
            connection.setRequestProperty("accept", "application/json");
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            StringBuffer response = new StringBuffer();

            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line).append('\n');
            }
            return response.toString();
        } finally {
            if (reader == null) {
                reader.close();
            }
        }
    }

    public static String postJson(String targetUrl, String data){
        return uploadJson(targetUrl, data, "POST");
    }
    public static String uploadJson(String targetURL, String data, String method) {
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);

            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.write(data.getBytes("utf-8"));

            //wr.writeUTF(urlParameters);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuilder response = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {

            return e.toString();

        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
